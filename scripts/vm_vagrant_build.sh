#!/bin/bash
#
# Script that run's within VM to do a full build test, called by vagrant_build.sh
#
# Fixtures don't like vagrant shared drive so creating a working copy.
echo "Syncing vagrant directory to /vagrant.spec..."
rsync -a /vagrant/ /vagrant.spec/

cd /vagrant.spec
  rake validate_puppet_code
    if [ $? -gt 0 ]; then exit; fi
  rake validate_ruby_code
    if [ $? -gt 0 ]; then exit; fi
  rake validate_erb_code
    if [ $? -gt 0 ]; then exit; fi

cd site/profile
  rake lint
    if [ $? -gt 0 ]; then exit; fi
  rake spec
    if [ $? -gt 0 ]; then exit; fi
