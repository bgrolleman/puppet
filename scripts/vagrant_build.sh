#!/bin/bash
#
# Start the build script within a virtual machine to do a quick test before uploading code to gitlab
#
vagrant ssh -c 'sudo /vagrant/scripts/vm_vagrant_build.sh' $1
