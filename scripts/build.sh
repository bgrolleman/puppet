#!/bin/bash
#
# Default Build script, does rsync to server and runs run.sh
SERVER=$1

# Sync Code
rsync -a --progress . $1:~/puppet.sync

# Update modules
ssh -t $1 "cd ~/puppet.sync; r10k puppetfile install -v"
ssh -t $1 "cd ~/puppet.sync; ./scripts/run.sh $2 $3 $4 $5"