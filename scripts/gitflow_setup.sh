#!/bin/sh
git branch production

cat >> .git/config <<EOF
[gitflow "branch"]
	master = production
	develop = development
[gitflow "prefix"]
	feature = feature/
	release = release/
	hotfix = hotfix/
	support = support/
	versiontag =
EOF
