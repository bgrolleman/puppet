#!/bin/bash
#
# Run puppet with apply to configure a machine without puppetmaster
#
# Usage: ./scripts/puppet_apply_run.sh
# Testing: ./scripts/puppet_apply_run.sh --noop
#
pushd `dirname $0`/.. > /dev/null
sudo /usr/bin/puppet apply --hiera_config="`pwd`/hiera.yaml" --modulepath="`pwd`/site:`pwd`/modules" `pwd`/manifests/site.pp   -v $@
popd > /dev/null
