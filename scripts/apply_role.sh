#!/bin/bash
#
# This command is used to run puppet, based on the hostname
pushd `dirname $0`/.. > /dev/null
if [ $# -lt 1 ]; then
  echo "Usage $0 <role>"
else
echo "include role::$1" | sudo /usr/bin/puppet apply --hiera_config="`pwd`/hiera.yaml" --modulepath="`pwd`/modules:`pwd`/site" -v $2 $3 $4 $5 $6 $7 $8
fi
popd > /dev/null
