#!/bin/bash
#
# Setup a puppetmaster without the need of an existing one 
#
if [ `facter lsbdistrelease` -eq '14.04' ]; then 
  cd /root
  sudo wget https://apt.puppetlabs.com/puppetlabs-release-trusty.deb
  sudo /usr/bin/dpkg -i puppetlabs-release-trusty.deb
  sudo apt-get update -qq
  sudo /usr/bin/apt-get install puppet -y -qq
  sudo /usr/bin/gem install r10k rake puppet-lint rspec-puppet puppetlabs_spec_helper
  # TODO Setup r10k and do initial run
else
  echo "Only run this on Ubuntu 14.04 for now"
fi
