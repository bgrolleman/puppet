#!/bin/bash
#
# Install puppet client on a new machine
#
if [ `facter lsbdistrelease` -eq '14.04' ]; then 
  cd /root
  sudo wget https://apt.puppetlabs.com/puppetlabs-release-trusty.deb
  sudo /usr/bin/dpkg -i puppetlabs-release-trusty.deb
  sudo apt-get update -qq
  sudo /usr/bin/apt-get install puppet -y -qq
  # Do initial run against puppet master
else
  echo "Only run this on Ubuntu 14.04 for now"
fi
