#!/bin/bash
pushd `dirname $0`/.. > /dev/null
sudo /usr/bin/puppet apply --hiera_config="`pwd`/hiera.yaml" --modulepath="`pwd`/modules:`pwd`/site" `pwd`/manifests/site.pp   -v $@
popd > /dev/null
