# Class: profile::mysql
class profile::mysql (
  $databases = {}
) {
  class { '::mysql::server':; }
  class { '::mysql::client':; }

  create_resources('::mysql::db',$databases)

  file { '/etc/prebackup.d/mysqldump.sh':
    source => "puppet:///modules/${module_name}/mysqldump.sh",
    owner  => 'root',
    group  => 'root',
    mode   => '0700',
  }
}
