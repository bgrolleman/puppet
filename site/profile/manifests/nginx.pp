# Class: profile::nginx
#
#  Configure nginx
#
class profile::nginx (
  $vhosts = {}
) {
  include ::nginx


  file { '/var/www':
    ensure => directory
  }

  create_resources('::nginx::resource::vhost', $vhosts, {
    use_default_location => false,
    include_files => ['defaults.phpsite']
  })
}
