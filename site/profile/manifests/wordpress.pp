# Class: profile::wordpress
#
# The wordpress profile is based on nginx
#
### Hiera Example
#
# Generate Wordpress site for example.com, www.example.com,
# example.com.wordpress.emendo-it.nl
# ```
# profile::wordpress::wordpress_sites:
#   example.com: {}
# ```
#
# Same but now add extra.com and www.extra.com
# ```
# profile::wordpress::wordpress_sites:
#   example.com:
#     domains:
#       - extra.com
# ```
class profile::wordpress (
  $wordpress_sites = {},
  $wordpress_plugins = [],
  $admin_user = 'admin',
  $admin_password = 'admin',
  $admin_email = 'root@localhost.locadomain',
  $default_domains = ['wordpress.emendo-it.nl']
) {
  include profile::nginx
  # PHP needs to be included within the role to select 5 or 7
  include profile::mysql
  include profile::wordpress::wpcli
  include wget

  create_resources('profile::wordpress::site',$wordpress_sites, {
    admin_user     => $admin_user,
    admin_password => $admin_password,
    admin_email    => $admin_email,
    require        => Class['profile::mysql']
  })

  wget::fetch { 'https://wordpress.org/latest.tar.gz':
    destination => '/root/wordpress.tar.gz',
    cache_dir   => '/var/cache/wget',
    cache_file  => 'latest.tar.gz'
  }
}
