# Class: profile::php5
#
# Install PHP5 Packages
#
class profile::php5 (
  $upload_max_filesize = '128M'
) {
  package { [ 'php5-cli', 'php5-cgi', 'php5-mysql', 'php5-gd', 'php5-curl' ]:
    ensure => installed
  }

  File {
    owner => 'root',
    group => 'root'
  }
  file {
    '/etc/init.d/php-fastcgi':
      mode   => '0755',
      source => 'puppet:///modules/profile/php5/php-fastcgi';
    '/etc/rc2.d/S99php-fastcgi':
      ensure => 'link',
      target => '/etc/init.d/php-fastcgi';
    ['/etc/php5/cgi/conf.d/99-puppet.ini', '/etc/php5/cli/conf.d/99-puppet.ini']:
      ensure => 'link',
      target => '/etc/php5/puppet.ini';
    '/etc/php5/puppet.ini':
      content => "[PHP]
upload_max_filesize = ${upload_max_filesize}
post_max_filesize= ${upload_max_filesize}
post_max_size = ${upload_max_filesize}
max_execution_time = 300
";
    '/etc/nginx/defaults.phpsite':
      mode   => '0644',
      source => "puppet:///modules/${module_name}/php5/defaults.phpsite";
  }
  service { 'php-fastcgi':
    ensure    => 'running',
    hasstatus => false,
    status    => '/usr/bin/pgrep php-cgi',
    require   => [File['/etc/init.d/php-fastcgi'],Package['php5-cgi']]
  }
}
