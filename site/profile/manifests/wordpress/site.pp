# define profile::wordpress::sit"profile::wordpress::wordpress_sites
#
# Setup single site
#
define profile::wordpress::site (
  $ensure         = 'present',
  $database       = $name,
  $domains        = [],
  $blocked        = false,
  $workdir        = "/var/www/${name}",
  $site_title     = $name,
  $admin_user     = 'admin',
  $admin_password = 'admin',
  $admin_email    = 'bgrolleman@emendo-it.nl',
  $url            = $name,
  $allowip        = [],
  $themes         = {},
  $ssl            = false
) {
  include ::profile::wordpress
  include stdlib
  $plugins = prefix($::profile::wordpress::wordpress_plugins,"${name}:")
  $all_domains = union([$name], prefix($::profile::wordpress::default_domains,"${name}."),$domains)
  $www_domains = prefix($all_domains,'www.')

  nginx::resource::vhost { "${name}.wordpress":
    ensure               => $ensure,
    www_root             => $workdir,
    server_name          => union($all_domains,$www_domains),
    use_default_location => false,
    raw_append           => template("${module_name}/wordpress/append.erb");
  }

  File {
    owner => 'www-data',
    group => 'www-data'
  }
  Exec {
    cwd  => $workdir,
    path => ['/usr/local/bin','/usr/bin']
  }

  file {$workdir:
    ensure  => directory,
    recurse => true,
    ignore  => 'wp-content',
  }

  exec { "wp core download ${name}":
    command => 'wp core download',
    user    => 'www-data',
    creates => "${workdir}/index.php",
  } ->
  exec { "wp core config ${name}":
    command => "wp core config  --dbname=${database}",
    user    => 'www-data',
    creates => "${workdir}/wp-config.php",
  } ->
  mysql_database { $database:
    ensure  => 'present',
    charset => 'utf8'
  } ->
  exec { "wp core installed ${name}":
    command => "wp core install --url='${url}' \
      --title='${site_title}' --admin_user='${admin_user}' \
      --admin_password='${admin_password}' \
      --admin_email='${admin_email}'",
    user    => 'www-data',
    unless  => 'wp core is-installed',
  } ->
  profile::wordpress::plugin { $plugins:}
  create_resources('profile::wordpress::site::theme', $themes, { workdir => $workdir })
}
