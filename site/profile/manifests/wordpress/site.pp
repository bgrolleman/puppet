# define profile::wordpress::sit"profile::wordpress::wordpress_sites
#
# Setup single site
#
define profile::wordpress::site (
  $ensure         = 'present',
  $database       = $name,
  $domains        = [],
  $blocked        = false,
  $workdir        = "/var/www/${name}",
  $site_title     = $name,
  $admin_user     = 'admin',
  $admin_password = 'admin',
  $admin_email    = 'bgrolleman@emendo-it.nl',
  $url            = $name,
  $allowip        = [],
  $themes         = {},
  $ssl_redirect   = true,
  $ssl            = true,
  $owner          = 'www-data',
  $group          = 'www-data',
  $dirs           = [],
  $manual         = false,
  $php_configfile = 'defaults.phpsite',
  $nginx_extra    = ''
) {
  include ::profile::wordpress
  include stdlib
  $plugins = prefix($::profile::wordpress::wordpress_plugins,"${name}:")
  $all_domains = union([$name], prefix($::profile::wordpress::default_domains,"${name}."),$domains)
  $www_domains = prefix($all_domains,'www.')

  if ( $ssl ) { 
    exec { "/usr/bin/certbot certonly --agree-tos -m bgrolleman@emendo-it.nl -n --nginx -d ${name}":
      creates => "/etc/letsencrypt/live/${name}/fullchain.pem"
    } -> nginx::resource::server { "${name}.wordpress":
      ensure               => $ensure,
      www_root             => $workdir,
      server_name          => union($all_domains,$www_domains),
      use_default_location => false,
      ssl_redirect         => $ssl_redirect,
      ssl                  => $ssl,
      ssl_cert             => "/etc/letsencrypt/live/${name}/fullchain.pem",
      ssl_key              => "/etc/letsencrypt/live/${name}/privkey.pem",
      raw_append           => template("${module_name}/wordpress/append.erb");
    }
  } else {
    nginx::resource::server { "${name}.wordpress":
      ensure               => $ensure,
      www_root             => $workdir,
      server_name          => union($all_domains,$www_domains),
      use_default_location => false,
      raw_append           => template("${module_name}/wordpress/append.erb");
    }
  }

  File {
    owner => $owner,
    group => $group
  }
  Exec {
    cwd  => $workdir,
    path => ['/usr/local/bin','/usr/bin']
  }

  file {$dirs:
    ensure => directory
  }

  file {$workdir:
    ensure  => directory,
    recurse => true,
    ignore  => 'wp-content',
  }

  unless ( $manual ) {
    exec { "wp core download ${name}":
      command => 'wp core download',
      user    => 'www-data',
      creates => "${workdir}/index.php",
    }
    -> mysql::db { $database:
      ensure   => 'present',
      charset  => 'utf8',
      user     => $database,
      password => $database
    }
    -> exec { "wp core config ${name}":
      command => "wp core config  --dbname=${database} --dbuser=${database} --dbpass=${database}",
      user    => 'www-data',
      creates => "${workdir}/wp-config.php",
    }
    -> exec { "wp core installed ${name}":
      command => "wp core install --url='${url}' \
        --title='${site_title}' --admin_user='${admin_user}' \
        --admin_password='${admin_password}' \
        --admin_email='${admin_email}'",
      user    => 'www-data',
      unless  => 'wp core is-installed',
    }
    -> profile::wordpress::plugin { $plugins:
    }
    create_resources('profile::wordpress::site::theme', $themes, { workdir => $workdir })
  }

#  letsencrypt::certonly { $database:
#    domains => union($all_domains,$www_domains),
#    server_name          =>
#    www_root             => $workdir,
#  domains => ['foo.example.com', 'bar.example.com'],
#  manage_cron => true,
#  cron_before_command => 'service nginx stop',
#  cron_success_command => '/bin/systemctl reload nginx.service',
#  suppress_cron_output => true,
# }
}
