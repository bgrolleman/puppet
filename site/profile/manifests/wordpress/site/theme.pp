# define to fetch / update theme
#
define profile::wordpress::site::theme (
  $source,
  $workdir,
  $branch = 'master',
  $ensure = 'present'
) {
  case $ensure {
    'present': {
      exec { "git clone ${source} ${workdir}/wp-content/themes/${name}":
        creates => "${workdir}/wp-content/themes/${name}"
      }
    }
    'absent': {
      file { "${workdir}/wp-content/themes/${name}":
        ensure  => 'absent',
        recurse => true
      }
    }
    default: {
      # Do Nothing
    }
  }
}
