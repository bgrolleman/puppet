# define profile::wordpress::plugin 
#
# Fetch plugins and install to wordpress
#
define profile::wordpress::plugin {
  $a = split($name,':')
  $site = $a[0]
  $plugin = $a[1]

  exec {"WP_Plugin_Get_${site}_${plugin}":
    command => "/usr/local/bin/wp plugin install ${plugin}",
    cwd     => "/var/www/${site}",
    unless  => "/usr/local/bin/wp plugin is-installed ${plugin}",
    user    => 'www-data',
    require => Class['profile::nginx'],
  }

}
