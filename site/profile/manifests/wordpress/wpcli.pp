# Class: profile::wordpress::wpcli
#
# Install Wordpress CLI Tool
#
class profile::wordpress::wpcli {
  wget::fetch { 'https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar':
    destination => '/usr/local/bin/wp',
  }
  file { '/usr/local/bin/wp':
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
    require => Wget::Fetch['https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar']
  }
}
