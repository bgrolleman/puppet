#
# profile::ajenti
#
### Authors
#
# * Bas Grolleman <bgrolleman@emendo-it.nl>
#
class profile::ajenti () {
  class {'::ajenti':; }
}
