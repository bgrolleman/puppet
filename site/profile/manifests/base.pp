# Profile: profile::base
#
# Profile with base configuration for all machines
#
# System based on autoloading hiera
#
# == Usage (Hiera)
#
# profile::base::packages:
#   - vim-nox
#
# profile::base::users:
#   username:
#     ensure: present
#     fullname: John Doe
#     email: john@doe.com
#
class profile::base (
  $packages = ['tmux'],
  $users = {}
) {

  package { $packages:
    ensure => installed;
  }
  create_resources('profile::user',$users)

  file { '/etc/vim/vimrc.local':
    source => 'puppet:///modules/profile/vimrc.local'
  }
  if $environment == 'vagrant' {
    file_line { 'Show Vagrant':
      path => '/root/.bashrc',
      line => 'PS1="VAGRANT $PS1"'
    }
  }
}
