# Create a single user
define profile::user (
  $ensure     = 'present',
  $fullname   = $name,
  $email      = "${name}@emendo-it.nl",
  $password   = undef,
  $editor     = 'vim',
  $groups     = [],
  $pubkeys    = {},
  $git        = true,
  $uid        = undef,
  $gid        = undef,
  $home       = "/home/${name}",
  $root       = false,
  $managehome = true,
  $shell      = '/bin/bash',
  $sudo       = false,
) {
  include ::stdlib

  user { $name:
    ensure     => $ensure,
    gid        => $gid,
    groups     => $groups,
    home       => $home,
    managehome => $managehome,
    password   => $password,
    shell      => $shell,
    uid        => $uid
  }
  File {
    ensure  => $ensure,
    owner => $name,
    group => $gid,
    mode  => '0600',
    require => User[$name]
  }
  file {
    [$home, "${home}/.ssh"]:
      ensure => directory,
      owner  => $name,
      mode   => '0755';
    "${home}/.bashrc":
      content => template("${module_name}/user/bashrc.erb");
  }
  if ( $git ) {
    file { "${home}/.gitconfig":
      content => template("${module_name}/user/gitconfig.erb");
    }
  }
  create_resources('ssh_authorized_key',$pubkeys,{
    user    => $name,
    ensure  => $ensure,
    require => File["${home}/.ssh"]
  })
  if ( $sudo ) {
    file { "/etc/sudoers.d/${name}":
      content => "${name} ALL=(ALL:ALL) NOPASSWD:ALL",
      owner   => 'root',
      group   => 'root',
      mode    => '0440'
    }
  }
  # TODO - Need Prefix function for hash
  #if ( $root == true ) {
  #  create_resources('ssh_authorized_key',prefix($pubkeys,'root'),{
  #    user    => 'root',
  #    ensure  => $ensure
  #  })
  #}
}
