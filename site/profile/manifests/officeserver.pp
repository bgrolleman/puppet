class profile::officeserver () {
  package {[
    'xubuntu-desktop',
    'samba',
    'chromium-browser'
  ]:
    ensure => installed
  }
}
