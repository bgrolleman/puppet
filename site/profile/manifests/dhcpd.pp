# Configure DHCP Server
#
# Limited to AbunaiSRV for now, it's a bit hackisch but I don't plan on doing this often anyway. ;) 
class profile::dhcpd {
  package { 'isc-dhcp-server': ensure => 'present' }
  package { 'dnsmasq': ensure => 'present' }

  File {
    notify => Service['isc-dhcp-server']
  }

  file { '/etc/dhcp/dhcpd.conf':
    content => '
ddns-update-style none;
option domain-name "abunai-srv.abunaicon.nl";
option routers 192.168.69.1;
option domain-name-servers 192.168.69.1;

default-lease-time 600;
max-lease-time 7200;

authoritative;
log-facility local7;

subnet 192.168.69.0 netmask 255.255.255.0 {
  range 192.168.69.10 192.168.69.199;
}'
  }

  file { '/etc/default/isc-dhcp-server':
    content => 'INTERFACES="eth1"'
  }

  service { 'isc-dhcp-server':
    ensure => 'running'
  }

  # You should add this ot /etc/rc.local or something 
  exec { '/sbin/iptables -t nat -A POSTROUTING -s 192.168.69.0/24 -o eth0 -j MASQUERADE':; }
}
