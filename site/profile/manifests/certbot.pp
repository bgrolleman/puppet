# Class: profile::certbot
#
# Install Certbot and scripts to manage let's encrypt ssl
#
class profile::certbot() {
  package { ['certbot','python3-certbot-nginx']:
    ensure => present
  }
  cron { 'Renew Certificates':
    command => '/usr/bin/certbot renew',
    user    => 'root',
    weekday => 2,
    hour    => 3,
    minute  => 10
  }
}
