# Class: profile::lxc
#
# Install / Configure lxc
#
class profile::lxc {
  package { 'lxc':
    ensure => installed
  }
}
