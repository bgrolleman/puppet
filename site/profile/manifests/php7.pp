# Class: profile::php7
#
# Install PHP7 Packages
#
class profile::php7 (
  $upload_max_filesize = '128M'
) {
  package { [ 'php7.0', 'php7.0-cli', 'php7.0-fpm', 'php7.0-mysql', 'php7.0-gd', 'php7.0-curl' ]:
    ensure => installed
  }

  File {
    owner => 'root',
    group => 'root'
  }

  file {
    '/etc/php/7.0/fpm/conf.d/90-puppet.ini':
      content => "[PHP]
upload_max_filesize = ${upload_max_filesize}
post_max_filesize= ${upload_max_filesize}
max_execution_time = 300
";
    '/etc/nginx/defaults.phpsite':
      mode   => '0644',
      source => "puppet:///modules/${module_name}/php7/defaults.phpsite";
  }
  
}
