# Setup IWP Cron job 
class profile::iwp {
  cron { 'iwp':
    command => '/usr/bin/php -q -d safe_mode=Off /var/www/iwp.emendo-it.nl/cron.php >/dev/null 2>&1',
    minute  => '*/20',
    hour    => '*',
  }

}
