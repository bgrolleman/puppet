require 'spec_helper'

describe 'profile::base', :type => 'class' do
  let(:title) { 'profile::base' }

  it { should contain_package('tmux') }

end
