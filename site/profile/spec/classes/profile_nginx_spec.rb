require 'spec_helper'

describe 'profile::nginx', :type => 'class' do
  let(:title) { 'profile::nginx' }
  let(:facts) {{
    :kernel          => 'linux',
    :operatingsystem => 'Ubuntu',
    :osfamily        => 'Debian',
    :lsbdistid       => 'Ubuntu',
    :lsbdistcodename => 'trusty'
  }}

  it { should contain_class('nginx') }
end
