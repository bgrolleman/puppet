#!/bin/sh
#
# Create Seperate MySQL Dumps per database using utf8
#
mkdir -p /opt/mysqlbackup
for db in $( mysql -Bse "show databases" | grep -v "_schema" ); do
	mysqldump -u root --opt --default-character-set=utf8 --skip-lock-tables -N $db | gzip > /opt/mysqlbackup/$db.sql.gz 2> /dev/null
done

