#
# Simple module to configure rsnapshot
#
# Author: Bas Grolleman <bgrolleman@emendo-it.nl>
#
class rsnapshot (
  $hosts = []
) {
  package { 'rsnapshot':
    ensure => installed
  }
  File {
    ensure  => 'file',
    mode    => '0644',
    owner   => 'root',
    group   => 'root',
  }
  file {
    '/etc/rsnapshot.conf':
      content => template('rsnapshot/rsnapshot.conf.erb');
    '/etc/prebackup.sh':
      source => "puppet:///modules/${module_name}/prebackup.sh",
      mode   => '0755';
    '/etc/prebackup.d':
      ensure => 'directory',
      mode   => '0755';
    '/etc/cron.d/rsnapshot':
      source  => 'puppet:///modules/rsnapshot/etc/cron.d/rsnapshot',
  }
}
