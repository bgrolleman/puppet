#
# Install / Configure Ajenti
#
### Authors
#
# * Bas Grolleman <bgrolleman@emendo-it.nl>
#
class ajenti () {
  include apt
  apt::key { '425E018E23944B4B42814EE0BDC3FBAA53029759':
    source => 'http://repo.ajenti.org/debian/key'
  } ->
  apt::source { 'ajenti':
    location => 'http://repo.ajenti.org/ng/debian',
    release  => 'main',
    repos    => 'main'
  } ->
  package {
    ['ajenti', 'ajenti-v', 'ajenti-v-nginx','ajenti-v-mysql', 'ajenti-v-php-fpm']:
      ensure => present
  } ~>
  service { 'ajenti':
    ensure => running
  }

}
