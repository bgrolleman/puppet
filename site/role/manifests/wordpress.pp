# Class:: role::wordpress
#
#
class role::wordpress {
  include ::profile::base
  include ::profile::nginx
  include ::profile::wordpress
  include ::profile::php5
  include ::mysql::server
  include ::nagios
  include ::rsnapshot
} # Class:: role::wordpress
