# Class:: role::abunaisrv
#
class role::abunaisrv {
  include ::profile::base
  include ::profile::ajenti
  include ::profile::nginx
  include ::profile::php5
  include ::profile::mysql
  include ::profile::officeserver
  #include ::nagios
  include ::rsnapshot
} # Class:: role::abunaisrv
