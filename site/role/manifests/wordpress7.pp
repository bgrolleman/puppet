# Class:: role::wordpress
#
# Wordpress with PHP7
#
class role::wordpress7 {
  include ::profile::base
  include ::profile::nginx
  include ::profile::wordpress
  include ::profile::php7
  include ::mysql::server
  include ::nagios
  include ::rsnapshot
} # Class:: role::wordpress
