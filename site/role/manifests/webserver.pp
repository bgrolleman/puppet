# Class:: role::webserver
#
#
class role::webserver {
  include ::profile::base
  include ::profile::nginx
  include ::profile::php5
  include ::mysql::server
  include ::nagios
  include ::rsnapshot
} # Class:: role::webserver
