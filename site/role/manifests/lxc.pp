# Class:: role::lxc
#
# Create LXC Server
#
class role::lxc {
  include ::profile::base
  include ::profile::lxc
  include ::profile::nginx
  include ::rsnapshot
  include ::profile::mysql
}
