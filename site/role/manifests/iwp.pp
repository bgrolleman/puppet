# Setup IWP Webserver
class role::iwp {
  include ::profile::base
  include ::profile::nginx
  include ::profile::mysql
  include ::profile::php5
  include ::profile::iwp
  include ::rsnapshot
}
