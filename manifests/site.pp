node /^ops/         { include ::role::ops }
node /^lxc/         { include ::role::lxc }
node /^wordpress/   { include ::role::wordpress }
node /wpdev$/       { include ::role::wordpress }
node 'puppetmaster' { include ::role::puppetmaster }
node 'abunai-srv'   { include ::role::abunaisrv }
node 'gaslicht'     { class { 'class_webserver':; }}

node /^abunaidev/ {
  include ::role::webserver

  nginx::wp_site { 'dev.abunaicon.nl':
    database => 'abunai';
  }
}

node default {
  include stdlib
  $role = hiera('role')
  notify { "Host: ${fqdn}":; }
  notify { "System Role: ${role}":; }
  if (empty($role)) {
    notify { 'No role configured?':; }
  } else {
    class { "::role::${role}":; }
  }
}
