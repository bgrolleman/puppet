# Puppet Control Repo

This module is my base puppet control repo, it provides the following

* Vagrantfile config per branch to quickly test config
* Auto deployment of r10k modules, within vagrant
* Setup can be deployed by r10k on puppetmaster
* Roles/Profiles configuration in "site" directory
* Hiera structure
* Automated build tests using gitlab-ci 

## Usage
Quick Start

* Install vagrant, git, virtualbox
* Clone repository
* vagrant up 

Should give you a base box

## Git Flow
Once base box is up and running, you can work on a single node using gitflow

* Install git flow
* git flow init (pick all defaults)
* git flow feature start <name>
* Add entry to vagrantfile starting with feature\_name
* vagrant up

## Running Build tests
./scripts/test.sh <hostname>

Should use a virtualbox image to run a full set of tests

